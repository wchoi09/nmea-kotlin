package com.example.nmeakotlin

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.GnssStatus
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.OnNmeaMessageListener
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.nmeakotlin.databinding.FragmentNmeaBinding
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import kotlin.math.floor

class NmeaFragment : Fragment(), OnNmeaMessageListener, LocationListener {

    private val TAG = "NMEA"

    private var binding: FragmentNmeaBinding? = null
    private val fragmentNmeaBinding
        get() = binding!!
    private var nmeaRmcTv: TextView? = null
    private var nmeaGgaTv: TextView? = null
    private var nmeaOtherTv: TextView? = null
    // private var mLocationManager: LocationManager? = null
    private lateinit var mLocationManager: LocationManager
    private val handler = Handler(Looper.getMainLooper())

    private val PERMISSIONS =
        arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

    private var lastNmeaMessage: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNmeaBinding.inflate(inflater, container, false)
        return fragmentNmeaBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!hasPermission()) {
            requestPermissionLauncher.launch(PERMISSIONS)
        }

        mLocationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        try {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0f, this)
            mLocationManager.registerGnssStatusCallback(mStatusCallback, handler)
            mLocationManager.addNmeaListener(this, handler)
            lastNmeaMessage = ""
        } catch (e: SecurityException) {
            Log.w(TAG, "Unable to open GPS", e)
        }

        nmeaRmcTv = binding!!.tvNmeaGprmc
        nmeaGgaTv = binding!!.tvNmeaGpgga
        nmeaOtherTv = binding!!.tvNmeaOther
    }

    override fun onResume() {
        super.onResume()
        try {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)
            mLocationManager.registerGnssStatusCallback(mStatusCallback, handler)
            mLocationManager.addNmeaListener(this, handler)
        } catch (e: SecurityException) {
            Log.w(TAG, "Unable to open GPS", e)
        }
    }

    override fun onPause() {
        super.onPause()
        mLocationManager.removeUpdates(this)
        mLocationManager.unregisterGnssStatusCallback(mStatusCallback)
        mLocationManager.removeNmeaListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
        mLocationManager.removeUpdates(this)
        mLocationManager.unregisterGnssStatusCallback(mStatusCallback)
        mLocationManager.removeNmeaListener(this)
    }

    private val mStatusCallback: GnssStatus.Callback =
        object : GnssStatus.Callback() {
            override fun onStarted() {}
            override fun onStopped() {}
            override fun onFirstFix(ttffMillis: Int) {}
            override fun onSatelliteStatusChanged(status: GnssStatus) {
                Log.v(TAG, String.format("Number of satellites: %d", status.satelliteCount))
            }
        }

    /** Report raw NMEA messages */
    override fun onNmeaMessage(message: String?, timestamp: Long) {
        // Log.v(TAG, message ?: "null")
        if (message != null) {
            lastNmeaMessage = message

            val nmeaList = message.split(",", "*").toTypedArray()
            if (lastNmeaMessage!!.startsWith("GPGGA", 1)) {
                nmeaGgaTv?.text = lastNmeaMessage
            } else if (lastNmeaMessage!!.startsWith("GPRMC", 1)) {
                Log.v(TAG, String.format("NMEA: %s", lastNmeaMessage))
                nmeaRmcTv?.text = lastNmeaMessage
                logRmc(nmeaList)
            } else {
                nmeaOtherTv!!.text = lastNmeaMessage
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        Log.v(TAG, String.format("Location update: %s", location))
        println("PARSED USING LOCATION LISTENER")
        Log.v(TAG, String.format("LATITUDE: %f", location.latitude))
        Log.v(TAG, String.format("LONGITUDE: %f", location.longitude))
    }

    private fun logRmc(rmcArr: Array<String>) {
        println("PARSED USING NMEA LISTENER")
        logLatLong(rmcArr[4], rmcArr[3], rmcArr[6], rmcArr[5])
        printDateTime(rmcArr[9], rmcArr[1])
    }

    private fun printDateTime(dateStr: String, timeStr: String) {
        // Convert date and time strings to LocalDateTime object
        val dateTimeStr = dateStr + timeStr
        val dateTime = LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ofPattern("ddMMyyHHmmss"))
        val utcZonedDateTime = ZonedDateTime.of(dateTime, ZoneId.of("UTC"))

        // Convert to KST timezone
        val kstZoneId = ZoneId.of("Asia/Seoul")
        val kstZonedDateTime = utcZonedDateTime.withZoneSameInstant(kstZoneId)

        // Convert back to LocalDateTime
        val kstDateTime = kstZonedDateTime.toLocalDateTime()

        // Format date-time string
        val formattedKstZonedDateTime =
            kstDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
        val formattedUtcZonedDateTime =
            utcZonedDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
        Log.v(TAG, String.format("UTC: %s", formattedUtcZonedDateTime))
        Log.v(TAG, String.format("KST: %s", formattedKstZonedDateTime))
    }

    private fun logLatLong(
        latDirStr: String,
        latitudeStr: String,
        longDirStr: String,
        longitudeStr: String
    ) {
        var latDeg: Float
        var longDeg: Float
        latDeg = latitudeStr.toFloat()
        longDeg = longitudeStr.toFloat()
        latDeg = (floor((latDeg / 100).toDouble()) + latDeg % 100 / 60.0f).toFloat()
        longDeg = (floor((longDeg / 100).toDouble()) + longDeg % 100 / 60.0f).toFloat()
        if (latDirStr == "S") {
            latDeg *= -1f
        }
        if (longDirStr == "W") {
            longDeg *= -1f
        }
        Log.v(TAG, String.format("LATITUDE: %s", latDeg))
        Log.v(TAG, String.format("LONGITUDE: %s", longDeg))
    }

    // Check for permissions
    private fun hasPermission(): Boolean {
        for (permission in PERMISSIONS) {
            if (
                ContextCompat.checkSelfPermission(requireContext(), permission) !=
                    PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGranted
            ->
            if (isGranted.containsValue(true)) {
                Toast.makeText(requireContext(), "Permission request granted", Toast.LENGTH_LONG)
                    .show()
            } else {
                Toast.makeText(requireContext(), "Permission request denied", Toast.LENGTH_LONG)
                    .show()
            }
        }
    companion object {}
}
